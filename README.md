Content manager
=====================================

Content manager allows you to simply aggregate data from different sources. 
It handles data from text file, json format and rss.

## Requirements
* php >= 5.6
* composer
* npm to run bower

## Installation

1. Clone repository
2. Run `bin/build` in console
3. Run `localhost:8000` in your browser.
4. Configure vhosts of your server or use built-in server running `php app/console server:start` in console and `localhost:8000` in browser

## How it works

There is **Container builder** which builds **Container** that represents content manager. 
**Container** is dependent on configuration which can be found in `src/ContentBundle/Resources/config/config.yml`.
It is already configured for example. It can be replaced by any other implementation of configuration reader.

**Container** consists of **Contents** - on frontend it is represented by tabs. 
Data of **Content** (tab) is individually read from defined source (text file, json, rss or yaml). 
Source can be provided from url or file path Location.

####Available readers
* File - read data from file format with simple regex parser
* Json - read data from json format
* Rss - read data from rss format (xml)
* Yaml - read data from yaml format

####Available readers' locations
* FilePath
* Url

**Content** consists of **Sections** - on frontend it is represented by table in tab.
For every section you can define its fields that should be presented in table on frontend.
You can use a bunch of **Transformers** to prepare data for **Section**.

####Available data transformers
* Concat - concat values with defined separator of two fields and write it to target field
* FormatDate - convert and format date in using format Y-m-d H:i:s
* FormatUrl - display url using anchor html tag
* GroupBy - groups data by defined field and write count of specific grouped values to target field
* OrderBy - sort data by defined field
* Limit - limit data to defined limit of rows
* HostnameExporter - parse defined field url value, extract hostname and write to target field
* RemoveField - hide field for displaying

## Example

Here is a complete example of configuration:

```yaml
config:
  varnish:
    title: 'Varnish Log'
    reader:
      type: file
      arguments: '/(?<ip>[0-9\.]+) - - \[(?<created_at>.*?)\] "([A-Z]+) (?<url>http.*) (HTTP.*)" ([0-9]{3}) ([0-9]+) "(?<hostname>.*)" "(.*)"/'
    location:
      type: filePath
      arguments: '__ROOT_DIR__/../var/data/varnish.log'
    sections:
      most_hosts:
        title: 'Hostnames with the most traffic'
        fields:
          sum:
            map: sum
            title: 'Traffic'
          hostname:
            map: hostname
            title: 'Host'
          url:
            map: url
            title: 'Url'
        transformers:
          host_name_exporter:
            type: hostnameExporter
            arguments: [url, hostname]
          group_by:
            type: groupBy
            arguments: [hostname, sum]
          order_by:
            type: orderBy
            arguments: [sum, desc, number]
          limit:
            type: limit
            arguments: 5
          remove_sum:
            type: remove
            arguments: sum
          remove_url:
            type: remove
            arguments: url
      most_files:
        title: 'Most requested files'
        fields:
          sum:
            map: sum
            title: 'Sum'
          url:
            map: url
            title: 'Url'
        transformers:
          group_by:
            type: groupBy
            arguments: [url, sum]
          order_by:
            type: orderBy
            arguments: [sum, desc, number]
          limit:
            type: limit
            arguments: 5
          remove_sum:
            type: remove
            arguments: sum
          format_url:
            type: formatUrl
            arguments: url
  rss:
    title: 'Rss'
    reader:
      type: rss
    location:
      type: url
      arguments: http://www.vg.no/rss/nyfront.php?frontId=1
    sections:
      articles:
        title: 'New articles'
        fields:
          title:
            map: title
            title: 'Title'
          publishedAt:
            map: pubDate
            title: 'Published at'
        transformers:
          format_date:
            type: formatDate
            arguments: pubDate
          order_by:
            type: orderBy
            arguments: [pubDate, desc, string]
  json:
    title: 'Json'
    reader:
      type: json
    location:
      type: url
      arguments: http://rexxars.com/playground/testfeed/
    sections:
      articles:
        title: 'Articles'
        fields:
          title:
            map: title
            title: Title
          link:
            map: link
            title: Link
          description:
            map: description
            title: Description
          publishedAt:
            map: publishedAt
            title: Published at
          date:
            map: date
            title: Date
          time:
            map: time
            title: Time
          category:
            map: category
            title: Category
        transformers:
          concat_date_and_time:
            type: concat
            arguments: [date, time, publishedAt, ' ']
          remove_date:
            type: remove
            arguments: date
          remove_time:
            type: remove
            arguments: time
          format_date:
            type: formatDate
            arguments: publishedAt
          order_by:
            type: orderBy
            arguments: [publishedAt, desc, string]
          format_url:
            type: formatUrl
            arguments: link
```

## Tests

To run tests you have to copy `app/phpunit.xml.dist` to `app/phpunit.xml` and set KERNEL_DIR variable in it.

## License

Released under the [MIT License](LICENSE).
