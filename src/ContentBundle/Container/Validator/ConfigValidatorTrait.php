<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Validator;


use ContentBundle\Container\Exception\InvalidConfigException;

trait ConfigValidatorTrait
{
    public function validateConfigNodeExists(array $config, $nodeName)
    {
        if (!array_key_exists($nodeName, $config)) {
            throw new InvalidConfigException(sprintf(
                'Node %s does not exist in config.',
                $nodeName
            ));
        }
    }

    public function validateConfigNodeIsArray(array $config, $nodeName)
    {
        if (!is_array($config[$nodeName])) {
            throw new InvalidConfigException(sprintf(
                'Node %s is not an array.',
                $nodeName
            ));
        }
    }
}
