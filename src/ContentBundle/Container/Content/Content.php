<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Content;

use Assert\Assertion;
use ContentBundle\Container\Section\Section;
use ContentBundle\Utils\ArrayCollectionCastTrait;
use Doctrine\Common\Collections\ArrayCollection;

class Content
{
    use ArrayCollectionCastTrait;

    /** @var string */
    private $title;
    /** @var ArrayCollection|Section[] $sections */
    private $sections;

    /**
     * @param string $title
     * @param Section[]|ArrayCollection $sections
     */
    public function __construct($title, ArrayCollection $sections)
    {
        $this->setTitle($title);
        $this->setSections($sections);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Section[]|ArrayCollection
     */
    public function getSections()
    {
        return $this->sections;
    }

    private function setTitle($title)
    {
        Assertion::string($title);
        Assertion::notEmpty($title);
        $this->title = $title;
    }

    private function setSections(ArrayCollection $sections)
    {
        $this->sections = $this->makeCollectionOfValid($sections, Section::class);
    }
}
