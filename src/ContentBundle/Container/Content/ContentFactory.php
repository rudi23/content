<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Content;

use ContentBundle\Container\Section\SectionFactoryInterface;
use ContentBundle\Container\Validator\ConfigValidatorTrait;
use ContentBundle\Location\LocationFactory;
use ContentBundle\Location\LocationFactoryInterface;
use ContentBundle\Reader\ReaderFactory;
use ContentBundle\Reader\ReaderFactoryInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ContentFactory implements ContentFactoryInterface
{
    use ConfigValidatorTrait;

    const TITLE_NODE = 'title';
    const SECTIONS_NODE = 'sections';
    const READER_NODE = 'reader';
    const LOCATION_NODE = 'location';

    /** @var SectionFactoryInterface */
    private $sectionFactory;
    /** @var ReaderFactory */
    private $readerFactory;
    /** @var LocationFactory */
    private $locationFactory;

    /**
     * @param SectionFactoryInterface $sectionFactory
     * @param ReaderFactoryInterface $readerFactory
     * @param LocationFactoryInterface $locationFactory
     */
    public function __construct(
        SectionFactoryInterface $sectionFactory,
        ReaderFactoryInterface $readerFactory,
        LocationFactoryInterface $locationFactory
    ) {
        $this->sectionFactory = $sectionFactory;
        $this->readerFactory = $readerFactory;
        $this->locationFactory = $locationFactory;
    }

    public function create(array $config)
    {
        $this->validate($config);

        $data = $this->createData($config);

        return new Content(
            $config[self::TITLE_NODE],
            $this->createSections($config, $data)
        );
    }

    private function createData(array $config)
    {
        $location = $this->locationFactory->create($config[self::LOCATION_NODE]);
        $reader = $this->readerFactory->create($config[self::READER_NODE]);
        $results = $reader->read($location);

        return $results;
    }

    private function createSections(array $config, array $data)
    {
        $sections = new ArrayCollection();
        foreach ($config[self::SECTIONS_NODE] as $sectionConfig) {
            $sections->add($this->sectionFactory->create($sectionConfig, $data));
        }

        return $sections;
    }

    private function validate(array $config)
    {
        $this->validateConfigNodeExists($config, self::TITLE_NODE);
        $this->validateConfigNodeExists($config, self::SECTIONS_NODE);
        $this->validateConfigNodeIsArray($config, self::SECTIONS_NODE);
        $this->validateConfigNodeExists($config, self::READER_NODE);
    }
}
