<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Content;

interface ContentFactoryInterface
{
    /**
     * @param array $config
     *
     * @return Content
     */
    public function create(array $config);
}
