<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container;

use ContentBundle\Container\Content\Content;
use ContentBundle\Utils\ArrayCollectionCastTrait;
use Doctrine\Common\Collections\ArrayCollection;

class Container
{
    use ArrayCollectionCastTrait;

    /** @var Content[]|ArrayCollection */
    private $contents;

    /**
     * @param ArrayCollection $contents
     */
    public function __construct(ArrayCollection $contents)
    {
        $this->setContents($contents);
    }

    /**
     * @return Content[]|ArrayCollection
     */
    public function getContents()
    {
        return $this->contents;
    }

    private function setContents(ArrayCollection $sections)
    {
        $this->contents = $this->makeCollectionOfValid($sections, Content::class);
    }
}
