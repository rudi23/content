<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container;

use ContentBundle\Container\Content\ContentFactoryInterface;
use ContentBundle\Container\Validator\ConfigValidatorTrait;
use ContentBundle\Location\LocationInterface;
use ContentBundle\Reader\ReaderInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ContainerBuilder
{
    use ConfigValidatorTrait;

    const ROOT_NAME = 'config';

    /** @var ReaderInterface */
    private $readerInterface;
    /** @var ContentFactoryInterface */
    private $contentFactory;

    public function __construct(ReaderInterface $readerInterface, ContentFactoryInterface $contentFactory)
    {
        $this->readerInterface = $readerInterface;
        $this->contentFactory = $contentFactory;
    }

    public function build(LocationInterface $location)
    {
        $config = $this->readerInterface->read($location);
        $this->validate($config);

        $contents = new ArrayCollection();
        foreach($config[self::ROOT_NAME] as $contentConfig){
            $contents->add($this->contentFactory->create($contentConfig));
        }

        return new Container($contents);
    }

    private function validate(array $config)
    {
        $this->validateConfigNodeExists($config, self::ROOT_NAME);
        $this->validateConfigNodeIsArray($config, self::ROOT_NAME);
    }
}
