<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Exception;

class InvalidConfigException extends \Exception
{
}
