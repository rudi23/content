<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Field;

class Field
{
    /** @var string */
    private $title;
    /** @var string */
    private $map;

    /**
     * @param $title
     * @param $map
     */
    public function __construct($title, $map)
    {
        $this->title = $title;
        $this->map = $map;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getMap()
    {
        return $this->map;
    }
}
