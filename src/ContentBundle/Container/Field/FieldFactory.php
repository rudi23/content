<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Field;

use ContentBundle\Container\Validator\ConfigValidatorTrait;

class FieldFactory implements FieldFactoryInterface
{
    use ConfigValidatorTrait;

    const TITLE_NODE = 'title';
    const MAP_NODE = 'map';

    public function create(array $config)
    {
        $this->validate($config);

        return new Field(
            $config[self::TITLE_NODE],
            $config[self::MAP_NODE]
        );
    }

    private function validate(array $config)
    {
        $this->validateConfigNodeExists($config, self::TITLE_NODE);
        $this->validateConfigNodeExists($config, self::MAP_NODE);
    }
}
