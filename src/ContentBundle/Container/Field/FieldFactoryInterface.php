<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Field;

interface FieldFactoryInterface
{
    /**
     * @param array $config
     *
     * @return Field
     */
    public function create(array $config);
}
