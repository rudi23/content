<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Section;

use Assert\Assertion;
use ContentBundle\Container\Field\Field;
use ContentBundle\Utils\ArrayCollectionCastTrait;
use Doctrine\Common\Collections\ArrayCollection;

class Section
{
    use ArrayCollectionCastTrait;

    /** @var string */
    private $title;
    /** @var Field[]|ArrayCollection */
    private $fields;
    /** @var array */
    private $data;

    /**
     * @param string          $title
     * @param ArrayCollection $fields
     * @param array           $data
     */
    public function __construct($title, ArrayCollection $fields, array $data)
    {
        $this->setTitle($title);
        $this->fields = $this->makeCollectionOfValid($fields, Field::class);
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Field[]|ArrayCollection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    private function setTitle($title)
    {
        Assertion::string($title);
        Assertion::notEmpty($title);
        $this->title = $title;
    }
}
