<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Section;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Field\Field;
use ContentBundle\Container\Field\FieldFactoryInterface;
use ContentBundle\Container\Validator\ConfigValidatorTrait;
use ContentBundle\Container\Transformer\TransformerFactoryInterface;
use ContentBundle\Container\Transformer\TransformerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class SectionFactory implements SectionFactoryInterface
{
    use ConfigValidatorTrait;

    const TITLE_NODE = 'title';
    const FIELDS_NODE = 'fields';
    const TRANSFORMERS_NODE = 'transformers';

    /** @var FieldFactoryInterface */
    private $fieldFactory;
    /** @var TransformerFactoryInterface */
    private $transformerFactory;

    /**
     * @param FieldFactoryInterface $fieldFactory
     * @param TransformerFactoryInterface $transformerFactory
     */
    public function __construct(
        FieldFactoryInterface $fieldFactory,
        TransformerFactoryInterface $transformerFactory
    ) {
        $this->fieldFactory = $fieldFactory;
        $this->transformerFactory = $transformerFactory;
    }

    /**
     * @param array $config
     * @param array $data
     * @return Section
     */
    public function create(array $config, array $data)
    {
        $this->validate($config);

        $fields = $this->createFields($config[self::FIELDS_NODE]);

        $result = $this->selectDataForFields($data, $fields);

        $transformers = $this->createTransformers($config);

        $result = $this->applyTransformers($transformers, $fields, $result);

        return new Section($config[self::TITLE_NODE], $fields, $result);
    }

    /**
     * @param array $fieldsConfig
     *
     * @return ArrayCollection|Field[]
     */
    private function createFields(array $fieldsConfig)
    {
        $fields = new ArrayCollection();
        foreach ($fieldsConfig as $fieldConfig) {
            $fields->add($this->fieldFactory->create($fieldConfig));
        }

        return $fields;
    }

    private function createTransformers(array $config)
    {
        $transformers = new ArrayCollection();

        if (array_key_exists(self::TRANSFORMERS_NODE, $config) && is_array($config[self::TRANSFORMERS_NODE])) {
            foreach ($config[self::TRANSFORMERS_NODE] as $transformerConfig) {
                $transformers->add($this->transformerFactory->create($transformerConfig));
            }
        }

        return $transformers;
    }

    /**
     * @param ArrayCollection|TransformerInterface[] $transformers
     * @param ArrayCollection|Field[] $fields
     * @param array $result
     * @return array
     */
    private function applyTransformers(ArrayCollection $transformers, ArrayCollection $fields, array $result)
    {
        foreach ($transformers as $transformer) {
            $result = $transformer->transform($fields, $result);
        }

        return $result;
    }

    /**
     * Validate configuration
     *
     * @param array $config
     *
     * @throws InvalidConfigException
     */
    private function validate(array $config)
    {
        $this->validateConfigNodeExists($config, self::FIELDS_NODE);
        $this->validateConfigNodeIsArray($config, self::FIELDS_NODE);
    }

    /**
     * Select data only for defined fields and in order of fields
     *
     * @param $data
     * @param ArrayCollection $fields
     * @return mixed
     * @throws InvalidConfigException
     */
    private function selectDataForFields($data, ArrayCollection $fields)
    {
        $fieldsMap = $this->getFieldsMap($fields);
        foreach ($data as &$row) {
            $newRow = [];
            foreach ($fieldsMap as $map) {
                $found = false;
                foreach ($row as $key => $value) {
                    if ($map == $key) {
                        $newRow[$map] = $value;
                        $found = true;
                    }
                }
                if (!$found) {
                    $newRow[$map] = '';
                }
            }
            $row = $newRow;
        }
        unset($row);

        return $data;
    }

    /**
     * Get mappers of all fields
     *
     * @param ArrayCollection|Field[] $fields
     * @return array
     */
    private function getFieldsMap(ArrayCollection $fields)
    {
        $fieldMap = [];
        foreach ($fields as $field) {
            $fieldMap[] = $field->getMap();
        }

        return $fieldMap;
    }
}
