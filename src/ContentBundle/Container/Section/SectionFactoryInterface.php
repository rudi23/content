<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Section;

interface SectionFactoryInterface
{
    /**
     * @param array $config
     * @param array $data
     *
     * @return Section
     */
    public function create(array $config, array $data);
}
