<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class FormatUrl implements TransformerInterface
{
    use FieldCheckTrait;

    /** @var string */
    private $urlField;

    /**
     * @param string $urlField
     */
    public function __construct($urlField)
    {
        $this->urlField = $urlField;
    }


    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->urlField, $fields);

            return $this->transformData($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function transformData(array $data)
    {
        foreach ($data as &$row) {
            $row[$this->urlField] = sprintf('<a href="%s">%s</a>', $row[$this->urlField], $row[$this->urlField]);
            unset($row);
        }

        return $data;
    }
}
