<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class GroupBy implements TransformerInterface
{
    use FieldCheckTrait;

    /** @var string */
    private $groupByFieldMap;
    /** @var string */
    private $countTargetFieldMap;

    /**
     * @param string $fieldGroupByMap
     * @param string $countTargetFieldMap
     */
    public function __construct($fieldGroupByMap, $countTargetFieldMap)
    {
        $this->groupByFieldMap = $fieldGroupByMap;
        $this->countTargetFieldMap = $countTargetFieldMap;
    }

    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->groupByFieldMap, $fields);
            $this->isInFields($this->countTargetFieldMap, $fields);

            $newData = [];
            foreach ($data as $row) {
                if (!isset($newData[$row[$this->groupByFieldMap]])) {
                    $newData[$row[$this->groupByFieldMap]] = $row;
                    $newData[$row[$this->groupByFieldMap]][$this->countTargetFieldMap] = 0;
                }
                $newData[$row[$this->groupByFieldMap]][$this->countTargetFieldMap] += 1;

            }

            return array_values($newData);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }
}
