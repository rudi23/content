<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;


interface TransformerFactoryInterface
{
    /**
     * @param array $config
     * @return TransformerInterface
     */
    public function create(array $config);
}
