<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class RemoveField implements TransformerInterface
{
    use FieldCheckTrait;

    /** @var string */
    private $fieldMap;

    /**
     * @param string $fieldMap
     */
    public function __construct($fieldMap)
    {
        $this->fieldMap = $fieldMap;
    }


    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->fieldMap, $fields);

            $this->removeField($fields);
            return $this->removeFieldData($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function removeField(ArrayCollection $fields)
    {
        foreach ($fields as $key => $field) {
            if ($field->getMap() === $this->fieldMap) {
                $fields->remove($key);
                break;
            }
        }
    }

    private function removeFieldData(array $data)
    {
        foreach ($data as &$row) {
            unset($row[$this->fieldMap]);
            unset($row);
        }

        return $data;
    }
}
