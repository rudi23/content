<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use Assert\Assertion;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use Doctrine\Common\Collections\ArrayCollection;

class Limit implements TransformerInterface
{
    const DEFAULT_LIMIT = 50;

    /** @var int */
    private $limit;

    /**
     * OrderBy constructor.
     * @param int $limit
     */
    public function __construct($limit = self::DEFAULT_LIMIT)
    {
        $this->setLimit($limit);
    }

    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        return array_slice($data, 0, $this->limit);
    }

    private function setLimit($limit)
    {
        Assertion::integer($limit);
        Assertion::greaterThan($limit, 0);

        $this->limit = $limit;
    }
}
