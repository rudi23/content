<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer\Helper;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Field\Field;
use Doctrine\Common\Collections\ArrayCollection;

trait FieldCheckTrait
{
    /**
     * @param string $fieldMap
     * @param ArrayCollection|Field[] $fields
     * @throws InvalidConfigException
     */
    protected function isInFields($fieldMap, ArrayCollection $fields)
    {
        $found = false;
        foreach ($fields as $field) {
            if ($field->getMap() === $fieldMap) {
                $found = true;
                break;
            }
        }

        if (!$found) {
            throw new InvalidConfigException(sprintf('Field map %s not found in defined fields.', $fieldMap));
        }
    }
}
