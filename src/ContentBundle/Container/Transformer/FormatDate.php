<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class FormatDate implements TransformerInterface
{
    use FieldCheckTrait;

    private $map = [
        'Januar' => 'January',
        'Februar' => 'February',
        'Mars' => 'March',
        'April' => 'April',
        'Mai' => 'May',
        'Juni' => 'June',
        'Juli' => 'July',
        'August' => 'August',
        'September' => 'September',
        'Oktober' => 'October',
        'November' => 'November',
        'Desember' => 'December',
    ];

    /** @var string */
    private $dateField;

    /**
     * @param string $dateField
     */
    public function __construct($dateField)
    {
        $this->dateField = $dateField;
    }


    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->dateField, $fields);

            return $this->transformData($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function transformData(array $data)
    {
        foreach ($data as &$row) {
            $row[$this->dateField] = $this->convertDate($row[$this->dateField]);
            unset($row);
        }

        return $data;
    }

    private function convertDate($originalDate)
    {
        $search = array_keys($this->map);
        $replace = array_values($this->map);
        $converted = str_replace($search, $replace, $originalDate);

        return date('Y-m-d H:i:s', strtotime($converted));
    }
}
