<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Transformer\Exception\TransformerException;
use Doctrine\Common\Collections\ArrayCollection;

interface TransformerInterface
{
    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data);
}
