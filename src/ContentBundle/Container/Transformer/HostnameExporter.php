<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class HostnameExporter implements TransformerInterface
{
    use FieldCheckTrait;

    /** @var string */
    private $urlField;
    /** @var string */
    private $hostnameField;

    /**
     * @param string $urlField
     * @param string $hostnameField
     */
    public function __construct($urlField, $hostnameField)
    {
        $this->urlField = $urlField;
        $this->hostnameField = $hostnameField;
    }

    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->urlField, $fields);
            $this->isInFields($this->hostnameField, $fields);

            return $this->transformData($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function transformData(array $data)
    {
        foreach ($data as &$row) {
            $hostnameValue = $row[$this->hostnameField];
            if (trim($hostnameValue) == '-') {
                $row[$this->hostnameField] = parse_url($row[$this->urlField], PHP_URL_HOST);
            }
            unset($row);
        }

        return $data;
    }
}
