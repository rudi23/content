<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Utils\InstantiatableObjectFactoryTrait;

class TransformerFactory implements TransformerFactoryInterface
{
    use InstantiatableObjectFactoryTrait;

    /**
     * @param array $classMap
     */
    public function __construct(array $classMap)
    {
        $this->setClassMap($classMap);
    }

    /**
     * @param array $config
     * @return TransformerInterface
     */
    public function create(array $config)
    {
        return $this->createInstance($config);
    }
}
