<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class Concat implements TransformerInterface
{
    use FieldCheckTrait;

    /** @var string */
    private $field1;
    /** @var string */
    private $field2;
    /** @var string */
    private $targetFieldMap;
    /** @var string */
    private $separator;

    /**
     * @param string $field1
     * @param string $field2
     * @param string $targetFieldMap
     * @param string $separator
     */
    public function __construct($field1, $field2, $targetFieldMap, $separator = ' ')
    {
        $this->field1 = $field1;
        $this->field2 = $field2;
        $this->targetFieldMap = $targetFieldMap;
        $this->separator = $separator;
    }


    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->field1, $fields);
            $this->isInFields($this->field2, $fields);
            $this->isInFields($this->targetFieldMap, $fields);

            return $this->transformData($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function transformData(array $data)
    {
        foreach ($data as &$row) {
            $row[$this->targetFieldMap] = $row[$this->field1] . $this->separator . $row[$this->field2];
            unset($row);
        }

        return $data;
    }
}
