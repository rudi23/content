<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Container\Transformer;

use Assert\Assertion;
use ContentBundle\Container\Exception\InvalidConfigException;
use ContentBundle\Container\Transformer\Exception\TransformerException;
use ContentBundle\Container\Transformer\Helper\FieldCheckTrait;
use Doctrine\Common\Collections\ArrayCollection;

class OrderBy implements TransformerInterface
{
    use FieldCheckTrait;

    const ASC = 'asc';
    const DESC = 'desc';

    const COMPARISON_STRING = 'string';
    const COMPARISON_NUMBER = 'number';

    /** @var string */
    private $orderByFieldMap;
    /** @var string */
    private $direction;
    /** @var string */
    private $comparisonType;

    /**
     * @param string $orderByFieldMap
     * @param string $direction
     * @param string $comparisonType
     */
    public function __construct($orderByFieldMap, $direction = self::ASC, $comparisonType = self::COMPARISON_STRING)
    {
        $this->orderByFieldMap = $orderByFieldMap;
        $this->setDirection($direction);
        $this->setComparisonType($comparisonType);
    }

    public static function getDirections()
    {
        return [self::ASC, self::DESC];
    }

    public static function getComparisonTypes()
    {
        return [self::COMPARISON_STRING, self::COMPARISON_NUMBER];
    }

    /**
     * @param ArrayCollection $fields
     * @param array $data
     * @return array
     * @throws TransformerException
     */
    public function transform(ArrayCollection $fields, array $data)
    {
        try {
            $this->isInFields($this->orderByFieldMap, $fields);

            return $this->sortResults($data);

        } catch (InvalidConfigException $e) {
            throw new TransformerException('Cannot apply transformer.', 0, $e);
        }
    }

    private function setDirection($direction)
    {
        Assertion::string($direction);
        $direction = trim($direction);
        Assertion::inArray($direction, self::getDirections());
        $this->direction = $direction;
    }

    private function setComparisonType($comparisonType)
    {
        Assertion::string($comparisonType);
        $comparisonType = trim($comparisonType);
        Assertion::inArray($comparisonType, self::getComparisonTypes());
        $this->comparisonType = $comparisonType;
    }

    private function sortResults($data)
    {
        $orderByFieldMap = $this->orderByFieldMap;
        $comparisonType = $this->comparisonType;

        usort($data, function ($currentItem, $nextItem) use ($orderByFieldMap, $comparisonType) {
            if ($comparisonType == self::COMPARISON_STRING) {
                return strcmp($currentItem[$orderByFieldMap], $nextItem[$orderByFieldMap]);
            } else {
                return $currentItem[$orderByFieldMap] > $nextItem[$orderByFieldMap];
            }
        });

        if ($this->direction === self::DESC) {
            $data = array_reverse($data);
        }

        return $data;
    }
}
