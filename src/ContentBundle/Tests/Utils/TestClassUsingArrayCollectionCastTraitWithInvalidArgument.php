<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Utils;

use ContentBundle\Utils\ArrayCollectionCastTrait;

class TestClassUsingArrayCollectionCastTraitWithInvalidArgument
{
    use ArrayCollectionCastTrait;

    public function __construct()
    {
        $this->makeCollectionOfValid(
            'string_that_should_be_array', TestA::class
        );
    }
}
