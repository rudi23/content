<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Utils;

class ArrayCollectionCastTraitTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInitialInvalidArgument()
    {
        new TestClassUsingArrayCollectionCastTraitWithInvalidArgument();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInitialInvalidArrayArgument()
    {
        new TestClassUsingArrayCollectionCastTraitWithInvalidInitialArray();
    }
}
