<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Utils;

use ContentBundle\Utils\ArrayCollectionCastTrait;

class TestClassUsingArrayCollectionCastTraitWithInvalidInitialArray
{
    use ArrayCollectionCastTrait;

    public function __construct()
    {
        $this->makeCollectionOfValid(
            [
                new TestA(),
                new TestB(),
            ], TestA::class
        );
    }
}
