<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Location;

use ContentBundle\Location\LocationFactory;
use ContentBundle\Location\Url;

class LocationFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $factory = $this->getLocationFactoryInstance();
        $location = $factory->create($this->getConfig());
        $this->assertInstanceOf(Url::class, $location);
    }

    /**
     * @expectedException \ContentBundle\Container\Exception\InvalidConfigException
     */
    public function testCreateWithWrongConfig()
    {
        $factory = $this->getLocationFactoryInstance();
        $factory->create($this->getWrongConfig());
    }

    public function testReplaceArguments()
    {
        $factory = $this->getLocationFactoryInstanceWithVariablesToReplace();
        $location = $factory->create($this->getConfig());

        $this->assertTrue(strpos($location->getRawData(), 'Wirtualna Polska') !== false);
    }

    private function getLocationFactoryInstance()
    {
        return new LocationFactory(['url' => Url::class], []);
    }
    private function getLocationFactoryInstanceWithVariablesToReplace()
    {
        return new LocationFactory(['url' => Url::class], ['onet' => 'wp']);
    }

    private function getConfig()
    {
        return ['type' => 'url', 'arguments' => 'http://www.onet.pl'];
    }

    private function getWrongConfig()
    {
        return [];
    }
}
