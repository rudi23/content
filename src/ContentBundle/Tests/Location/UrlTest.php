<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Location;

use ContentBundle\Location\Url;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    const URL = 'http://onet.pl';
    const URL_CONTENT_PART = '<title>Onet.pl</title>';

    public function testGetRawData()
    {
        $url = new Url(self::URL);
        $content = $url->getRawData();

        $this->assertTrue(strpos($content, self::URL_CONTENT_PART) !== false);
    }

    /**
     * @expectedException \ContentBundle\Location\Exception\HostConnectionException
     */
    public function testGetRawDataWithInvalidUrl()
    {
        $filePath = new Url('http://www.invalid-not-existing-address.com');
        $filePath->getRawData();
    }
}
