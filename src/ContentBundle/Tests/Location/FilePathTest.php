<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Location;

use ContentBundle\Location\FilePath;

class FilePathTest extends \PHPUnit_Framework_TestCase
{
    public function testGetRawData()
    {
        $filePath = new FilePath(__DIR__ . '/../var/example.txt');
        $content = $filePath->getRawData();

        $this->assertTrue(strpos($content, 'example_content') !== false);
    }

    /**
     * @expectedException \ContentBundle\Location\Exception\InvalidFilePathException
     */
    public function testGetRawDataWithInvalidFilePath()
    {
        $filePath = new FilePath(__DIR__ . '/../var/not_existing_example.txt');
        $filePath->getRawData();
    }
}
