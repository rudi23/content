<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Section;

use ContentBundle\Container\Field\Field;
use ContentBundle\Container\Section\Section;
use Doctrine\Common\Collections\ArrayCollection;

class SectionTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $this->getInstance();
    }

    public function testGetTitle()
    {
        $section = $this->getInstance();
        $this->assertTrue($section->getTitle() == 'title');
    }

    public function testGetData()
    {
        $section = $this->getInstance();
        $this->assertTrue($section->getData() == []);
    }

    public function testGetFields()
    {
        $section = $this->getInstance();
        $this->assertInstanceOf(ArrayCollection::class, $section->getFields());
        $this->assertInstanceOf(Field::class, $section->getFields()->first());
    }

    private function getInstance()
    {
        return new Section('title', $this->getFieldMockCollection(), []);
    }

    private function getFieldMockCollection()
    {
        $fieldMock = \Mockery::mock(Field::class);

        $sections = new ArrayCollection();
        $sections->add($fieldMock);

        return $sections;
    }
}
