<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Section;

use ContentBundle\Container\Field\Field;
use ContentBundle\Container\Field\FieldFactory;
use ContentBundle\Container\Field\FieldFactoryInterface;
use ContentBundle\Container\Section\SectionFactory;
use ContentBundle\Container\Transformer\TransformerFactoryInterface;

class SectionFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        return new SectionFactory(
            $this->getFieldFactoryMock(),
            $this->getTransformerFactoryMock()
        );
    }

    public function testCreateSection()
    {
        $config =
            [
                'title' => 'Title1',
                'fields' => [
                    [
                    ]
                ],
                'transformers' => [
                    [
                    ]
                ]
            ];

        $sectionFactory = $this->testCreate();
        $sectionFactory->create($config, []);
    }

    public function testPrepareDataForDefinedField()
    {
        $sectionFactory = new SectionFactory(
            new FieldFactory(),
            $this->getTransformerFactoryMock()
        );

        $config =
            [
                'title' => 'Title1',
                'fields' => [
                    [
                        'title' => 'field1',
                        'map' => 'field1'

                    ],
                    [
                        'title' => 'field2',
                        'map' => 'field2'

                    ],
                    [
                        'title' => 'field2',
                        'map' => 'field3'

                    ],
                ],
                'transformers' => []
            ];

        $inputData = [
            [
                'field2' => '',
                'field1' => '',
                'field4' => ''
            ]
        ];

        $expectedData = [
            [
                'field1' => '',
                'field2' => '',
                'field3' => ''
            ]
        ];

        $section = $sectionFactory->create($config, $inputData);

        $this->assertTrue($section->getData() == $expectedData);
    }

    private function getFieldFactoryMock()
    {
        $fieldMock = \Mockery::mock(Field::class)
            ->shouldReceive('getMap')
            ->once()
            ->andReturn(0)
            ->getMock();

        return \Mockery::mock(FieldFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn($fieldMock)
            ->getMock();
    }

    private function getTransformerFactoryMock()
    {
        $transformerMock = \Mockery::mock(Field::class)
            ->shouldReceive('transform')
            ->once()
            ->andReturn([])
            ->getMock();

        return \Mockery::mock(TransformerFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn($transformerMock)
            ->getMock();
    }
}
