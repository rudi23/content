<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Field;

use ContentBundle\Container\Field\FieldFactory;

class FieldFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        return new FieldFactory();
    }

    public function testCreateField()
    {
        $fieldFactory = $this->testCreate();
        $fieldFactory->create([
            'title' => 'Title',
            'map' => 'title'
        ]);
    }
}
