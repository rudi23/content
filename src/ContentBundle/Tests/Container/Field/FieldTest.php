<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Field;

use ContentBundle\Container\Field\Field;

class FieldTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $this->getInstance();
    }

    public function testGetTitle()
    {
        $field = $this->getInstance();
        $this->assertTrue($field->getTitle() == 'title');
    }

    public function testGetMap()
    {
        $field = $this->getInstance();
        $this->assertTrue($field->getMap() == 0);
    }

    private function getInstance()
    {
        return new Field('title', 0);
    }
}
