<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container;

use ContentBundle\Container\Container;
use ContentBundle\Container\Content\Content;
use Doctrine\Common\Collections\ArrayCollection;

class ContainerTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $this->getInstance();
    }

    public function testGetContents()
    {
        $container = $this->getInstance();
        $this->assertInstanceOf(ArrayCollection::class, $container->getContents());
        $this->assertInstanceOf(Content::class, $container->getContents()->first());
    }

    private function getInstance()
    {
        return new Container($this->getContentMockCollection());
    }

    private function getContentMockCollection()
    {
        $contentMock = \Mockery::mock(Content::class);

        $contents = new ArrayCollection();
        $contents->add($contentMock);

        return $contents;
    }
}
