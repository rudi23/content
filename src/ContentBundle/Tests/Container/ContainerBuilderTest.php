<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container;

use ContentBundle\Container\ContainerBuilder;
use ContentBundle\Container\Content\Content;
use ContentBundle\Container\Content\ContentFactoryInterface;
use ContentBundle\Location\LocationInterface;
use ContentBundle\Reader\ReaderInterface;

class ContainerBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        return new ContainerBuilder(
            $this->getReaderMock([]),
            $this->getContentFactoryMock()
        );
    }

    public function testBuild()
    {
        $config =
            [
                'config' => [
                    [],
                    []
                ]
            ];

        $containerBuilder = new ContainerBuilder(
            $this->getReaderMock($config),
            $this->getContentFactoryMock()
        );
        $containerBuilder->build($this->getLocationMock());
    }

    private function getContentFactoryMock()
    {
        return \Mockery::mock(ContentFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn(\Mockery::mock(Content::class))
            ->getMock();
    }

    private function getReaderMock($configToReturn)
    {
        return \Mockery::mock(ReaderInterface::class)
            ->shouldReceive('read')
            ->once()
            ->andReturn($configToReturn)
            ->getMock();
    }

    private function getLocationMock()
    {
        return \Mockery::mock(LocationInterface::class);
    }
}
