<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\HostnameExporter;

class HostnameExporterTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new HostnameExporter(self::FIELD1, self::FIELD2);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'http://www.example.com/path?googleguy=googley',
                self::FIELD2 => '-',
            ],
            [
                self::FIELD1 => 'http://www.example.com/path?googleguy=googley',
                self::FIELD2 => 'www.example.com',
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => 'http://www.example.com/path?googleguy=googley',
                self::FIELD2 => 'www.example.com',
            ],
            [
                self::FIELD1 => 'http://www.example.com/path?googleguy=googley',
                self::FIELD2 => 'www.example.com',
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new HostnameExporter(self::FIELD4, self::FIELD2);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
