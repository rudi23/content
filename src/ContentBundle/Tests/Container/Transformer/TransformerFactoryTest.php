<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\FormatDate;
use ContentBundle\Container\Transformer\TransformerFactory;
use ContentBundle\Container\Transformer\TransformerInterface;

class TransformerFactoryTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new TransformerFactory([
            'formatDate' => FormatDate::class
        ]);
    }

    public function testTransform()
    {
        $transformerFactory = $this->testCreate();

        $config = ['type' => 'formatDate', 'arguments' => self::FIELD1];

        $transformer = $transformerFactory->create($config);

        $this->assertInstanceOf(TransformerInterface::class, $transformer);
    }
}
