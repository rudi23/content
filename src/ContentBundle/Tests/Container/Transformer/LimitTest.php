<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\Limit;

class LimitTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new Limit(2);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => '1'
            ],
            [
                self::FIELD1 => '2'
            ],
            [
                self::FIELD1 => '3'
            ],
            [
                self::FIELD1 => '4'
            ],
            [
                self::FIELD1 => '5'
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => '1'
            ],
            [
                self::FIELD1 => '2'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }
}
