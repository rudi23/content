<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\FormatDate;

class FormatDateTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new FormatDate(self::FIELD1);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'March 10, 2001, 5:16 pm'
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => '2001-03-10 17:16:00'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new FormatDate(self::FIELD4);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
