<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\GroupBy;

class GroupByTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new GroupBy(self::FIELD1, self::FIELD2);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'abc',
                self::FIELD2 => ''
            ],
            [
                self::FIELD1 => 'def',
                self::FIELD2 => ''
            ],
            [
                self::FIELD1 => 'abc',
                self::FIELD2 => ''
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => 'abc',
                self::FIELD2 => 2
            ],
            [
                self::FIELD1 => 'def',
                self::FIELD2 => 1
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new GroupBy(self::FIELD4, self::FIELD2);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
