<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\RemoveField;

class RemoveFieldTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new RemoveField(self::FIELD1);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ],
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ],
            [
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new RemoveField(self::FIELD4);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
