<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\Concat;

class ConcatTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new Concat(self::FIELD1, self::FIELD2, self::FIELD3);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ],
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp3'
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp1 temp2'
            ],
            [
                self::FIELD1 => 'temp1',
                self::FIELD2 => 'temp2',
                self::FIELD3 => 'temp1 temp2'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new Concat(self::FIELD4, self::FIELD2, self::FIELD3);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
