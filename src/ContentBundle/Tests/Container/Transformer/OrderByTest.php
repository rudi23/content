<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\OrderBy;

class OrderByTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new OrderBy(self::FIELD1);
    }

    public function testTransformNumberDescending()
    {
        $data = [
            [
                self::FIELD1 => 2,
            ],
            [
                self::FIELD1 => 1
            ],
            [
                self::FIELD1 => 3
            ]
        ];

        $transformer = new OrderBy(self::FIELD1, OrderBy::DESC, OrderBy::COMPARISON_NUMBER);
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => 3
            ],
            [
                self::FIELD1 => 2
            ],
            [
                self::FIELD1 => 1
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    public function testTransformStringAscending()
    {
        $data = [
            [
                self::FIELD1 => 'xyz'
            ],
            [
                self::FIELD1 => 'abc',
            ],
            [
                self::FIELD1 => 'klm'
            ]
        ];

        $transformer = new OrderBy(self::FIELD1, OrderBy::ASC, OrderBy::COMPARISON_STRING);
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => 'abc',
            ],
            [
                self::FIELD1 => 'klm'
            ],
            [
                self::FIELD1 => 'xyz'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new OrderBy(self::FIELD4);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
