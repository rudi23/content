<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Transformer\FormatUrl;

class FormatUrlTest extends TransformerTestCaseAbstract
{
    public function testCreate()
    {
        return new FormatUrl(self::FIELD1);
    }

    public function testTransform()
    {
        $data = [
            [
                self::FIELD1 => 'http://www.onet.pl'
            ]
        ];

        $transformer = $this->testCreate();
        $data = $transformer->transform($this->getFieldsCollectionMock(), $data);

        $expectedData = [
            [
                self::FIELD1 => '<a href="http://www.onet.pl">http://www.onet.pl</a>'
            ]
        ];

        $this->assertTrue($data === $expectedData);
    }

    /**
     * @expectedException \ContentBundle\Container\Transformer\Exception\TransformerException
     */
    public function testInvalidTransform()
    {
        $transformer = new FormatUrl(self::FIELD4);
        $transformer->transform($this->getFieldsCollectionMock(), []);
    }
}
