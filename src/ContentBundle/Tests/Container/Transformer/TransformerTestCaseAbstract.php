<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Container\Transformer;

use ContentBundle\Container\Field\Field;
use Doctrine\Common\Collections\ArrayCollection;

abstract class TransformerTestCaseAbstract extends \PHPUnit_Framework_TestCase
{
    const FIELD1 = 'field1';
    const FIELD2 = 'field2';
    const FIELD3 = 'field3';
    const FIELD4 = 'field4';

    protected function getFieldsCollectionMock()
    {
        $coll = new ArrayCollection();
        $coll->add($this->getFieldMock(self::FIELD1));
        $coll->add($this->getFieldMock(self::FIELD2));
        $coll->add($this->getFieldMock(self::FIELD3));

        return $coll;
    }

    protected function getFieldMock($returnData)
    {
        return \Mockery::mock(Field::class)
            ->shouldReceive('getMap')
            ->once()
            ->andReturn($returnData)
            ->getMock();
    }
}
