<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Content;

use ContentBundle\Container\Content\ContentFactory;
use ContentBundle\Container\Section\Section;
use ContentBundle\Container\Section\SectionFactoryInterface;
use ContentBundle\Reader\ReaderFactoryInterface;
use ContentBundle\Reader\ReaderInterface;
use ContentBundle\Location\LocationInterface;
use ContentBundle\Location\LocationFactoryInterface;

class ContentFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        return new ContentFactory(
            $this->getSectionFactoryMock(),
            $this->getReaderFactoryMock(),
            $this->getLocationFactoryMock()
        );
    }

    public function testCreateContent()
    {
        $config =
            [
                'title' => 'Title1',
                'reader' => [],
                'location' => [],
                'sections' => [
                    []
                ]
            ];

        $contentFactory = $this->testCreate();
        $contentFactory->create($config, []);
    }

    /**
     * @expectedException \ContentBundle\Container\Exception\InvalidConfigException
     */
    public function testCreateContentWithInvalidConfig()
    {
        $config =
            [
                'title' => 'Title1',
                'reader' => [],
                'location' => [],
                'sections' => 'string_that_should_be_array'
            ];

        $contentFactory = $this->testCreate();
        $contentFactory->create($config, []);
    }

    private function getSectionFactoryMock()
    {
        $sectionMock = \Mockery::mock(Section::class)
            ->shouldReceive('getMap')
            ->once()
            ->andReturn(0)
            ->getMock();

        return \Mockery::mock(SectionFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn($sectionMock)
            ->getMock();
    }

    private function getReaderFactoryMock()
    {
        $readerMock = \Mockery::mock(ReaderInterface::class)
            ->shouldReceive('read')
            ->once()
            ->andReturn([])
            ->getMock();

        return \Mockery::mock(ReaderFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn($readerMock)
            ->getMock();
    }

    private function getLocationFactoryMock()
    {
        $locationMock = \Mockery::mock(LocationInterface::class)
            ->shouldReceive('getRawData')
            ->once()
            ->andReturn([])
            ->getMock();

        return \Mockery::mock(LocationFactoryInterface::class)
            ->shouldReceive('create')
            ->once()
            ->andReturn($locationMock)
            ->getMock();
    }

}
