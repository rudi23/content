<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Content;

use ContentBundle\Container\Content\Content;
use ContentBundle\Container\Section\Section;
use Doctrine\Common\Collections\ArrayCollection;

class ContentTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $this->getInstance();
    }

    public function testGetSections()
    {
        $content = $this->getInstance();
        $this->assertInstanceOf(ArrayCollection::class, $content->getSections());
        $this->assertInstanceOf(Section::class, $content->getSections()->first());
    }

    public function testGetTitle()
    {
        $content = $this->getInstance();
        $this->assertTrue($content->getTitle() == 'title');
    }

    private function getInstance()
    {
        return new Content('title', $this->getSectionMockCollection());
    }

    private function getSectionMockCollection()
    {
        $sectionMock = \Mockery::mock(Section::class);

        $sections = new ArrayCollection();
        $sections->add($sectionMock);

        return $sections;
    }
}
