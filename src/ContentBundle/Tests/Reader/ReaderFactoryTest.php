<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Reader\File;
use ContentBundle\Reader\ReaderFactory;

class ReaderFactoryTest extends ReaderTestCaseAbstract
{
    public function testCreate()
    {
        $factory = $this->getReaderFactoryInstance();
        $location = $factory->create($this->getConfig());
        $this->assertInstanceOf(File::class, $location);
    }

    private function getReaderFactoryInstance()
    {
        return new ReaderFactory(['file' => File::class]);
    }

    private function getConfig()
    {
        return ['type' => 'file', 'arguments' => $this->getFileRegex()];
    }
}
