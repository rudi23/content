<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Location\Exception\LocationException;
use ContentBundle\Location\LocationInterface;

abstract class ReaderTestCaseAbstract extends \PHPUnit_Framework_TestCase
{
    protected function getLocationMock($dataToReturn)
    {
        return \Mockery::mock(LocationInterface::class)
            ->shouldReceive('getRawData')
            ->once()
            ->andReturn($dataToReturn)
            ->getMock();
    }

    protected function getLocationMockWithException()
    {
        return \Mockery::mock(LocationInterface::class)
            ->shouldReceive('getRawData')
            ->once()
            ->andThrow(LocationException::class)
            ->getMock();
    }

    protected function getContentInArray()
    {
        return [
            ['Col1', 'Col2', 'Col3'],
            ['Col1', 'Col2', 'Col3'],
            ['Col1', 'Col2', 'Col3']
        ];
    }

    protected function getContentInIndexedArray()
    {
        return [
            ['col1' => 'Col1', 'col2' => 'Col2', 'col3' => 'Col3'],
            ['col1' => 'Col1', 'col2' => 'Col2', 'col3' => 'Col3'],
            ['col1' => 'Col1', 'col2' => 'Col2', 'col3' => 'Col3']
        ];
    }

    protected function getContentOfFile()
    {
        return "Col1 Col2 Col3\nCol1 Col2 Col3\nCol1 Col2 Col3";
    }

    protected function getFileRegex()
    {
        return '/([a-zA-Z0-9]+) ([a-zA-Z0-9]+) ([a-zA-Z0-9]+)/';
    }

    protected function getContentInYaml()
    {
        return <<<EOD
-
  - Col1
  - Col2
  - Col3
-
  - Col1
  - Col2
  - Col3
-
  - Col1
  - Col2
  - Col3
EOD;
    }

    protected function getContentInJson()
    {
        return json_encode($this->getContentInArray());
    }

    protected function getContentOfRss()
    {
        return <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:vg="http://www.vg.no/namespace">
<channel>
    <item>
        <col1>Col1</col1>
        <col2>Col2</col2>
        <col3>Col3</col3>
    </item>
    <item>
        <col1>Col1</col1>
        <col2>Col2</col2>
        <col3>Col3</col3>
    </item>
    <item>
        <col1>Col1</col1>
        <col2>Col2</col2>
        <col3>Col3</col3>
    </item>
</channel>
</rss>

EOD;
    }
}
