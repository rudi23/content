<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Reader\Rss;

class RssTest extends ReaderTestCaseAbstract
{
    public function testRead()
    {
        $location = $this->getLocationMock($this->getContentOfRss());

        $reader = new Rss();
        $content = $reader->read($location);

        $this->assertTrue($content == $this->getContentInIndexedArray());
    }

    public function testReadInvalid()
    {
        $reader = new Rss();
        $content = $reader->read($this->getLocationMockWithException());

        $this->assertTrue($content == []);
    }
}
