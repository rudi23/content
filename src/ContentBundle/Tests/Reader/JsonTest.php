<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Reader\Json;

class JsonTest extends ReaderTestCaseAbstract
{
    public function testRead()
    {
        $location = $this->getLocationMock($this->getContentInJson());

        $reader = new Json();
        $content = $reader->read($location);

        $this->assertTrue($content == $this->getContentInArray());
    }

    public function testReadInvalid()
    {
        $reader = new Json();
        $content = $reader->read($this->getLocationMockWithException());

        $this->assertTrue($content == []);
    }
}
