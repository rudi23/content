<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Reader\Yaml;

class YamlTest extends ReaderTestCaseAbstract
{
    public function testRead()
    {
        $location = $this->getLocationMock($this->getContentInYaml());

        $reader = new Yaml();
        $content = $reader->read($location);

        $this->assertTrue($content == $this->getContentInArray());
    }

    public function testReadInvalid()
    {
        $reader = new Yaml();
        $content = $reader->read($this->getLocationMockWithException());

        $this->assertTrue($content == []);
    }
}
