<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Tests\Reader;

use ContentBundle\Reader\File;

class FileTest extends ReaderTestCaseAbstract
{
    public function testRead()
    {
        $location = $this->getLocationMock($this->getContentOfFile());

        $reader = new File($this->getFileRegex());
        $content = $reader->read($location);

        $this->assertTrue($content == $this->getContentInArray());
    }

    public function testReadInvalid()
    {
        $reader = new File($this->getFileRegex());
        $content = $reader->read($this->getLocationMockWithException());

        $this->assertTrue($content == []);
    }

    public function testReadWithStringIndex()
    {
        $location = $this->getLocationMock("Col1 Col2 Col3");

        $reader = new File('/(?<field1>[a-zA-Z0-9]+) ([a-zA-Z0-9]+) ([a-zA-Z0-9]+)/');
        $content = $reader->read($location);

        $this->assertTrue($content == [['field1' => 'Col1', 0 => 'Col1', 1 => 'Col2', 2 => 'Col3']]);
    }
}
