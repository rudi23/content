<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Utils;

use Doctrine\Common\Collections\ArrayCollection;

trait ArrayCollectionCastTrait
{
    protected function makeCollectionOfValid($elements, $className)
    {
        $sanitized = new ArrayCollection();
        if (is_array($elements) || $elements instanceof ArrayCollection) {
            foreach ($elements as $key => $element) {
                if (is_object($element) && is_a($element, $className)) {
                    $sanitized->set($key, $element);
                } else {
                    throw new \InvalidArgumentException(
                        sprintf("Invalid collection passed - use only elements of type %s", $className)
                    );
                }
            }
            return $sanitized;
        } else {
            throw new \InvalidArgumentException(
                sprintf("Invalid collection passed - use ArrayCollection or array of %s", $className)
            );
        }
    }
}
