<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Utils;

use Assert\Assertion;
use ContentBundle\Container\Validator\ConfigValidatorTrait;

trait InstantiatableObjectFactoryTrait
{
    use ConfigValidatorTrait;

    protected $typeNode = 'type';
    protected $argumentsNode = 'arguments';

    /** @var array */
    protected $classMap;

    /**
     * @param array $classMap
     */
    protected function setClassMap(array $classMap)
    {
        $this->classMap = $classMap;
    }

    /**
     * @param array $config
     * @return object
     */
    protected function createInstance(array $config)
    {
        $this->validate($config);

        $instantiator = new Instantiator();

        return $instantiator->instantiate($this->getType($config), $this->getArguments($config));
    }

    private function validate(array $config)
    {
        $this->validateConfigNodeExists($config, $this->typeNode);
    }

    /**
     * @param array $config
     * @return string
     */
    private function getType(array $config)
    {
        $type = $config[$this->typeNode];

        Assertion::string($type);
        Assertion::inArray($type, array_keys($this->classMap));

        return $this->classMap[$type];
    }

    /**
     * @param array $config
     * @return array
     */
    private function getArguments(array $config)
    {
        $args = [];
        if (array_key_exists($this->argumentsNode, $config)) {
            $args = $config[$this->argumentsNode];
            if (!is_array($args)) {
                $args = (array) $args;
            }
        }

        return $args;
    }
}
