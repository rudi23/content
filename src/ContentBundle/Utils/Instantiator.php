<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Utils;

class Instantiator
{
    public function instantiate($className, $arguments = array())
    {
        $reflectionClass = new \ReflectionClass($className);
        return $reflectionClass->newInstanceArgs($arguments);
    }
}
