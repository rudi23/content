<?php
/**
 * @author krudowski
 */

namespace ContentBundle\Utils;

trait ArrayTrimRecursiveTrait
{
    public function trimRecursive(array $data)
    {
        array_walk_recursive($data, function (&$item) {
            if (is_string($item)) {
                $item = trim($item);
            }
        });

        return $data;
    }
}
