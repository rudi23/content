<?php

namespace ContentBundle\Location;

/**
 * @author krudowski
 */
interface LocationInterface
{
    /**
     * @return string
     */
    public function getRawData();
}
