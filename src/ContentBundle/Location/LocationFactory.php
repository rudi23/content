<?php

namespace ContentBundle\Location;

use ContentBundle\Utils\InstantiatableObjectFactoryTrait;

/**
 * @author krudowski
 */
class LocationFactory implements LocationFactoryInterface
{
    use InstantiatableObjectFactoryTrait;

    /** @var array */
    private $variablesToReplace;

    /**
     * @param array $classMap
     * @param array $variablesToReplace
     */
    public function __construct(array $classMap, array $variablesToReplace = array())
    {
        $this->setClassMap($classMap);
        $this->variablesToReplace = $variablesToReplace;
    }

    /**
     * @param array $config
     * @return LocationInterface
     */
    public function create(array $config)
    {
        return $this->createInstance($config);
    }

    /**
     * @param array $config
     * @return array
     */
    protected function getArguments(array $config)
    {
        $args = [];
        if (array_key_exists($this->argumentsNode, $config)) {
            $args = $config[$this->argumentsNode];
            if (!is_array($args)) {
                $args = (array) $args;
            }
        }

        return $this->replaceVariables($args);
    }

    private function replaceVariables($arguments)
    {
        foreach ($arguments as &$argument) {
            foreach ($this->variablesToReplace as $name => $value) {
                $argument = str_replace($name, $value, $argument);
            }
        }

        return $arguments;
    }
}
