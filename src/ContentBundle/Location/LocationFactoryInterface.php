<?php

namespace ContentBundle\Location;

/**
 * @author krudowski
 */
interface LocationFactoryInterface
{
    /**
     * @param array $config
     * @return LocationInterface
     */
    public function create(array $config);
}
