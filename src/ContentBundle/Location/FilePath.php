<?php

namespace ContentBundle\Location;

use Assert\Assertion;
use ContentBundle\Location\Exception\InvalidFilePathException;

/**
 * @author krudowski
 */
class FilePath implements LocationInterface
{
    /** @var string */
    private $filePath;

    /**
     * @param string $filePath
     */
    public function __construct($filePath)
    {
        $this->setFilePath($filePath);
    }

    public function getRawData()
    {
        if (!is_file($this->filePath) || !is_readable($this->filePath)) {
            throw new InvalidFilePathException();
        }

        return file_get_contents($this->filePath);
    }

    private function setFilePath($filePath)
    {
        Assertion::string($filePath);
        Assertion::notEmpty($filePath);

        $this->filePath = $filePath;
    }
}
