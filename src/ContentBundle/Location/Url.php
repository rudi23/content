<?php

namespace ContentBundle\Location;

use Assert\Assertion;
use ContentBundle\Location\Exception\HostConnectionException;

/**
 * @author krudowski
 */
class Url implements LocationInterface
{
    /** @var string */
    private $url;

    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->setUrl($url);
    }

    public function getRawData()
    {
        $content = @file_get_contents($this->url);
        if (false !== $content) {
            return $content;
        } else {
            throw new HostConnectionException();
        }
    }

    /**
     * @param $url
     */
    private function setUrl($url)
    {
        Assertion::url($url);
        $this->url = $url;
    }
}
