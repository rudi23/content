<?php
/**
 * @author krudowski
 */

namespace ContentBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class ContentExtension extends Extension
{
    const BUNDLE_DIR = '__BUNDLE_DIR__';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->replaceParametersBundleDirConstant($container);
    }

    private function replaceParametersBundleDirConstant(ContainerBuilder $container)
    {
        foreach ($container->getParameterBag()->all() as $key => $param) {
            if (is_string($param) && strpos($param, self::BUNDLE_DIR) !== false) {
                $container->setParameter($key,
                    str_replace(self::BUNDLE_DIR, __DIR__ . DIRECTORY_SEPARATOR . '..', $param)
                );
            }
        }
    }
}
