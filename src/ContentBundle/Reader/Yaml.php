<?php

namespace ContentBundle\Reader;

use ContentBundle\Location\Exception\LocationException;
use ContentBundle\Location\LocationInterface;
use Symfony\Component\Yaml\Parser;

/**
 * @author krudowski
 */
class Yaml implements ReaderInterface
{
    /**
     * @param LocationInterface $location
     * @return array
     */
    public function read(LocationInterface $location)
    {
        try {
            $parser = new Parser();

            return $parser->parse($location->getRawData());

        } catch (LocationException $e) {
            return array();
        }
    }
}
