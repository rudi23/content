<?php

namespace ContentBundle\Reader;

use ContentBundle\Location\LocationInterface;

/**
 * @author krudowski
 */
interface ReaderInterface
{
    /**
     * @param LocationInterface $location
     * @return array
     */
    public function read(LocationInterface $location);
}
