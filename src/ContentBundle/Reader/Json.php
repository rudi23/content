<?php

namespace ContentBundle\Reader;

use Assert\Assertion;
use ContentBundle\Location\Exception\LocationException;
use ContentBundle\Location\LocationInterface;
use ContentBundle\Utils\ArrayTrimRecursiveTrait;

/**
 * @author krudowski
 */
class Json implements ReaderInterface
{
    use ArrayTrimRecursiveTrait;

    /**
     * @param LocationInterface $location
     *
     * @return array
     */
    public function read(LocationInterface $location)
    {
        try {
            $content = $location->getRawData();
            Assertion::isJsonString($content);

            return $this->trimRecursive(json_decode($content, true));

        } catch (LocationException $e) {
            return array();
        }
    }
}
