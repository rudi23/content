<?php

namespace ContentBundle\Reader;

/**
 * @author krudowski
 */
interface ReaderFactoryInterface
{
    /**
     * @param array $config
     * @return ReaderInterface
     */
    public function create(array $config);
}
