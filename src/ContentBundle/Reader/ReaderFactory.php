<?php

namespace ContentBundle\Reader;

use ContentBundle\Utils\InstantiatableObjectFactoryTrait;

/**
 * @author krudowski
 */
class ReaderFactory implements ReaderFactoryInterface
{
    use InstantiatableObjectFactoryTrait;

    /**
     * @param array $classMap
     */
    public function __construct(array $classMap)
    {
        $this->setClassMap($classMap);
    }

    /**
     * @param array $config
     * @return ReaderInterface
     */
    public function create(array $config)
    {
        return $this->createInstance($config);
    }
}
