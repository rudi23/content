<?php

namespace ContentBundle\Reader;

use ContentBundle\Location\Exception\LocationException;
use ContentBundle\Location\LocationInterface;

/**
 * @author krudowski
 */
class File implements ReaderInterface
{
    /** @var string */
    private $regexPattern;

    /**
     * @param string $regexPattern
     */
    public function __construct($regexPattern)
    {
        $this->regexPattern = $regexPattern;
    }

    /**
     * @param LocationInterface $location
     *
     * @return array
     */
    public function read(LocationInterface $location)
    {
        try {
            $content = $location->getRawData();

            return $this->prepare($content);

        } catch (LocationException $e) {
            return array();
        }
    }

    /**
     * @param string $data
     * @return array
     */
    private function prepare($data)
    {
        $data = explode("\n", $data);
        $data = array_map('trim', $data);
        $data = array_filter($data);

        foreach ($data as &$line) {
            preg_match($this->regexPattern, $line, $matches);
            unset($matches[0]);
            $line = $this->changeIntegerKeys($matches);
            unset($line);
        }

        return $data;
    }

    private function changeIntegerKeys($matches)
    {
        $newMatches = [];
        foreach ($matches as $key => $value) {
            if (is_int($key)) {
                $newMatches[$key - 1] = $value;
            } else {
                $newMatches[$key] = $value;
            }
        }

        return $newMatches;
    }
}
