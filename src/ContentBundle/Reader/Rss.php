<?php

namespace ContentBundle\Reader;

use ContentBundle\Location\Exception\LocationException;
use ContentBundle\Location\LocationInterface;
use ContentBundle\Utils\ArrayTrimRecursiveTrait;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @author krudowski
 */
class Rss implements ReaderInterface
{
    use ArrayTrimRecursiveTrait;

    const RSS_DATA_PATH = '[channel][item]';

    /**
     * @param LocationInterface $location
     * @return array
     */
    public function read(LocationInterface $location)
    {
        try {
            $content = json_decode(json_encode(simplexml_load_string($location->getRawData())), true);

            $accessor = PropertyAccess::createPropertyAccessor();
            $content = $accessor->getValue($content, self::RSS_DATA_PATH);

            return $this->trimRecursive($content);

        } catch (LocationException $e) {
            return array();
        }
    }
}
