<?php

namespace AppBundle\Controller;

use ContentBundle\Reader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction()
    {
        $contentContainerBuilder = $this->get('content_container.builder');
        $configLocation = $this->get('content_container.config_location');

        $dataContainer = $contentContainerBuilder->build($configLocation);

        return $this->render('AppBundle:index:index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
            'data_container' => $dataContainer
        ));
    }
}
